FROM rust:latest

ENV RUST_BACKTRACE=1
ENV ARCH=aarch64-unknown-linux-gnu

RUN dpkg --add-architecture arm64
RUN apt-get update -yqq && apt-get -yqq install --no-install-recommends gcc make gcc-aarch64-linux-gnu binutils-aarch64-linux-gnu python3 pip

RUN pip install ziglang && \
    cargo install cargo-zigbuild && \
    rustup target add  aarch64-unknown-linux-gnu
