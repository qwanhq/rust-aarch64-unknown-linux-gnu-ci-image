# Rust Aarch64 Unknown Linux Gnu CI image

Rust on ARM64 Docker image for Gitlab CI projects. Cross-compile ARM64 targets on gitlab without docker-in-docker (dind). Aims to have as few moving parts as possible. Based on `rust:latest`.

## Motivation

We want to keep the energy footprint of our applications as low as possible. Running serverless (google firebase and AWS lambda ) helps, because a service that isnt' used doesn't use energy. ARM also helps, because it has good performance per watt (which is reflected in lower costs per lambda invocation). Rust allows us to build services that need less CPU and memory when they are invoked.

Efficient and fast CI, while investment up front and ongoing, saves us time, [each time](https://www.qwan.eu/2021/07/07/simple-acts-of-kindness-and-love.html) we test and deploy a service.

## Installation

### in gitlab-ci
Add to `.gitlab-ci.yml`, using [image from dockerhub](https://hub.docker.com/repository/docker/mostalive/rust-aarch64-unknown-linux-gnu):

```
image: "mostalive/rust-aarch64-unknown-linux-gnu:<week number>"
```

## Usage: local tinkering

Build the dockerfile locally, and then mount your project into a directory. e.g:

```
docker build . -t tinkering
cd <your-app-directory>
docker run -v $(pwd):/app --workdir /app tinkering make
```

Using `make` as an example, this can be any script. Gitlab CI seems to assume `/` as WORKDIR, and we can't mount a project in `/`, so we mount and set the WORKDIR on running.

## Support

Feel free to post an issue in the issue tracker, or contact us through qwan.eu. This is experimental, we're happy to consult with you on setting up something similar for your organisation.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

In no particular order (apart from the top one which is next**:

* Automate Docker build and push
* Build weekly, and number the builds - e.g. 202202, 202203
* Can we add and run a small sample gitlab-ci project in a subdirectory?
* Publish dockerfiles as the QWAN organisations somewhere. Dockerhub?
* Reduce CI build time by shrinking image file size by switching to Alpine Linux 
  Not likely to happen soon, it was hard enough to do this with Debian, which we know reasonably well.

If and when Gitlab offers native ARM64 builds, this repository will be deprecated. Rust runs and builds just fine on e.g. a PI, we hope running a whole pipeline in ARM64 will be enabled at some point.

### Roadmap next: automate docker build and push

[Building with kaniko](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html#building-a-docker-image-with-kaniko) looks promising, as it does not need docker-in-docker (simpler and faster). It creates a docker.json from (protected) environment variables, so we don't leak secrets in the source code.

## Tag format for weekly build: 

``` bash
> date "+%Y%V"
202202
```

## Tagg and push local image:

```bash
docker tag tinkering mostalive/rust-aarch64-unknown-linux-gnu:$(date "+%Y%V") 
```

Resulting image tag looks like `tinkering mostalive/rust-aarch64-unknown-linux-gnu:202202`

```bash
docker push mostalive/rust-aarch64-unknown-linux-gnu:$(date "+%Y%V") 
```

## Contributing

Suggestions for a simpler Docker file, faster builds of this docker file and associated Rust projects are welcome. We'd welcome tests (currently none).


## License
MIT license

## Project status

Experimental.
